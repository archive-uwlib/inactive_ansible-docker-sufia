This will git clone dockerfiles from https://bitbucket.org/uwlib/docker-sufia/overview

It will then build the following docker images from Dockerfiles.

  - tomcat:f4solr (this contains fedora4 and solr)
  - druw/hydrabase (this contains development stuff, fits, ffmpeg) 
  - druw (this contains rails, apache, passenger)
  - some stock redis that is built on run

It will then run 3 containers:

  - tomcat (For development fedora4, solr)
  - tomcat-p (For production fedora4, solr)
  - redis
---
Copy/rename and Edit the following before running:

  - `cp vars.yml.template vars.yml`
  - edit vars.yml
    - replace "/path/to/docker-sufia".
    - eg. /path/to/this/dir/ansible-docker-sufia/docker-sufia

To run this playbook:

`ansible-playbook -K -i inventory playbook.yml`

-K will ask for the machine sudo password. -i will pass the inventory file to use.

Reference
http://kreusch.com.br/blog/2013/12/03/manage-a-development-machine-with-ansible/